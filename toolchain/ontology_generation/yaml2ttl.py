#!/usr/bin/env python3
import sys

import fnmatch
import os
import yaml
from pathlib import Path
from typing import Dict, List
from colorama import Fore, Style
import re
from datetime import datetime
import pytz

def parseYAML(input) -> Dict:
    with open(input, "r") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return data


def string_escape(s, encoding='utf-8'):

    """
    A method used for escaping special chars in a string.
    """

    return (s.encode('latin1')  # To bytes, required by 'unicode-escape'
            .decode('unicode-escape')  # Perform the actual octal-escaping decode
            .encode('latin1')  # 1:1 mapping back to bytes
            .decode(encoding))  # Decode original encoding


def camel_case_split(str):
    """
    :param str:  A camelCase String.
    :return: a string that with spaces between camel Case.
    """
    strings = re.findall(r'[A-Za-z](?:[a-z]+|[A-Z]*(?=[AZ]|$))', str)

    result = ""
    for i, itm in enumerate(strings):
        result = result + itm
        if i + 1 < len(strings):
            result = result + " "

    return result


def generate_ontology_file(save_to: Path, name: str, yaml_base_files: List, simple_data_types: List, prefixes: List,
                           prefix_def_folder: Path):

    """
    Computes *.ttl files that describe the ontology  and saves them to save_to: Path

    :param save_to: path to folder where *.ttl files will be saved
    :param name: name of prefix
    :param yaml_base_files: paths of yaml files that are associated with prefix = name
    :param simple_data_types: list of simple dataTypes that distinguish owl:DatatypeProperty and owl:ObjectProperty
    :param prefixes: List of prefixes to add at the beginning
    :param prefix_def_folder: Folder that defines description of prefixes (not defined in yaml files)
    """

    ## distance between name and value.
    distance = "\t\t"

    # check namespace and produce warning if it is not from GAIA-X
    if "gax-" in name:
        file_name = str(name).split("-")[1] + "_generated.ttl"
    else:
        file_name = name + ".ttl"
        print(Fore.YELLOW + "WARNING during Ontology generation"
              + " -- " + " new ontology file with name: " + file_name + " " + Style.RESET_ALL)

    # parse list of Paths to list of filenames without full path
    base_files = [x.name for x in yaml_base_files]
    print("generating " + str(save_to / file_name) + " from " + str(base_files))
    ttlFile = open((save_to / file_name), "w")

    #### Definition prefeix
    for prefix in prefixes["Prefixes"]:
        line = "@prefix %s: <%s> .\n" % (prefix["name"], prefix["value"])
        ttlFile.write(line)

    end_of_line = "\n\n"
    ttlFile.write(end_of_line)


    # check existence of file
    prefix_def_files = fnmatch.filter(os.listdir(prefix_def_folder), "*.yaml")
    prefix_def_file = [file for file in prefix_def_files if name == str(file).split(".")[0]]
    assert len(prefix_def_file) == 1, "Please define a definition for " + name + " in the prefix definitions folder " + str(prefix_def_folder)

    pref_def_dic = parseYAML(prefix_def_folder / prefix_def_file[0])

    # generate prefix content
    line = name + ": \n"
    ttlFile.write(line)
    for itm in pref_def_dic['OntologyDescription']:
        itm_name = itm['name']
        value = itm['value']
        if isinstance(value, list):
           value = ', '.join(value)

        line = itm_name + distance + value + " ;\n"
        ttlFile.write(line)

    today = datetime.now(pytz.timezone('Europe/Paris')).isoformat(' ', 'seconds').replace(" ", "T")
    line = 'dct:modified' + distance + "\"" + today + "\"^^xsd:dateTimeStamp ;\n"


    ttlFile.write(line)

    end_of_line = ".\n\n"
    ttlFile.write(end_of_line)

    ## END of prefix context ##

    # generate yaml file content
    for file in yaml_base_files:
        yml_dic = parseYAML(file)
        for key in [*yml_dic.keys()]:
            class_name = key
            prefix = yml_dic[key]["prefix"]
            subclasses = yml_dic[key]["subClassOf"]
            assert prefix == name
            full_class_name = prefix + ":" + class_name

            ttlFile.write('################## \n##{0}\n##################\n\n'.format(class_name))

            ttlFile.write(full_class_name)

            l0 = "\n\ta{0} owl:Class;".format(distance)
            l1 = "\n\trdfs:label{0} \"{1}\"@en ;".format(distance, camel_case_split(class_name))

            l2 = ""
            if subclasses:
                l2 = "\n\trdfs:subClassOf{0}".format(distance)

                subclasses_modified = []
                for sub_class in subclasses:
                    if str(sub_class).startswith("http"):
                        subclasses_modified.append("<" + sub_class + ">")
                    else:
                        subclasses_modified.append( prefix + ":" + sub_class)

                l2 = l2 + ', '.join(subclasses_modified)
                l2 = l2 + " ;"

            line_list = [l0, l1, l2]
            for itm in line_list:
                ttlFile.write(itm)

            end_of_line = "\n. \n\n"
            ttlFile.write(end_of_line)

            for attribute in yml_dic[key]["attributes"]:

                title = attribute['title']
                prefix = attribute['prefix']
                dataType = attribute['dataType']
                # removes any quotes in string
                description = attribute['description'].replace('"','\\"')


                if "gax" in prefix:
                    li0 = prefix + ":" + title
                    if dataType in simple_data_types:
                        li1 = "\n\ta{0} owl:DatatypeProperty ;".format(distance)
                    else:
                        li1 = "\n\ta{0} owl:ObjectProperty ;".format(distance)

                    li2 = "\n\trdfs:label{0} \"{1}\"@en ;".format(distance, camel_case_split(title).lower())
                    li3 = "\n\trdfs:domain{0} {1} ;".format(distance, full_class_name)
                    li4 = "\n\trdfs:range{0} {1} ;".format(distance, dataType)
                    li5 = "\n\trdfs:comment{0} \"{1}\" ;".format(distance, description)

                    line_list = [li0, li1, li2, li3, li4, li5]
                    for itm in line_list:
                        ttlFile.write(itm)

                    end_of_line = "\n. \n\n"
                    ttlFile.write(end_of_line)
                else:
                    print("Escaping: " + prefix + ":" + title)

    ttlFile.close()

def find_matches(path: Path, prefixes: Dict) -> Dict:
    """ Returns a dic. containing a list of files that are associated with one prefix"""

    # creates output dictionary with expected prefixes
    keys = [p['name'] for p in prefixes["Prefixes"]]
    matches = dict(zip(keys, [None] * len(keys)))

    files = fnmatch.filter(os.listdir(path), "*.yaml")
    files = [f for f in files if f != "dataTypeAbbreviation.yaml" or f != "prefixes.yaml"]

    # get prefix of yaml file
    for file in files:
        file_path = path / file
        yaml_dic = parseYAML(file_path)

        name = list(yaml_dic.keys())
        prefix = yaml_dic[name[0]]['prefix']

        if prefix in keys:
            if matches[prefix] is None:
                matches[prefix] = [file_path]
            else:
                matches[prefix].append(file_path)
        else:
            print(Fore.RED + "ERROR during Ontology generation"
                  + " -- " + prefix + " undefined -> Add prefix to prefix file: /yaml/validation/prefixes.yaml " + Style.RESET_ALL)
            sys.exit(1)

    # remove prefixes that are not found
    for itm in list(matches.keys()):
        if matches[itm] is None:
            matches.pop(itm, None)

    return matches


if __name__ == "__main__":

    #ONTOLOGY_TARGET_FOLDER = ('../../tmp')
    ONTOLOGY_TARGET_FOLDER = ('../../yaml2ontology/')

    ONTOLOGY_TOOLCHAIN_FOLDER = Path('.')
    ONTOLOGY_DEF_FOLDER = ONTOLOGY_TOOLCHAIN_FOLDER / 'prefix_definitions'

    prefixFile = ONTOLOGY_TOOLCHAIN_FOLDER / 'prefixes.yaml'
    prefixes = parseYAML(prefixFile)

    prefix_def_folder = ONTOLOGY_TOOLCHAIN_FOLDER / 'prefix_definitions'

    dataTypeAbbreviation = Path('../../single-point-of-truth/yaml/validation/dataTypeAbbreviation.yaml')
    simple_data_types = list(parseYAML(dataTypeAbbreviation).keys())

    matches = find_matches(Path("../../allYaml/"), prefixes)

    print("Starting to generate ontology files (*.ttl)")
    for key in matches.keys():
        generate_ontology_file(save_to=Path(ONTOLOGY_TARGET_FOLDER), name=key, yaml_base_files=matches[key],
                                   simple_data_types=simple_data_types, prefixes=prefixes,
                                   prefix_def_folder=prefix_def_folder)
    print(Fore.GREEN + 'Successfully generated ontology' + Style.RESET_ALL)




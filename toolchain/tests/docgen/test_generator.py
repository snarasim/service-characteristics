from ddt import ddt, data
from unittest.mock import Mock

import docgen.sd_attribute_table as gen
import unittest


@ddt
class TestFGenerator(unittest.TestCase):

    @unittest.mock.patch('os.listdir')
    @data([], ['.yml', '.md'])
    def test_sort_files_by_name(self, extensions, list_dir):
        # set return types
        list_dir.return_value = ['file1.yaml', 'file_b.yml', 'file2.txt', 'file_a.md']

        # run test
        files_names = gen.sort_files_by_name("", extensions)

        # verify tests1
        if not extensions:
            self.assertEqual(['file1', 'file2', 'file_a', 'file_b'], files_names)
        else:
            self.assertEqual(['file_a', 'file_b'], files_names)

    def test_get_class_name_from_file(self):
        self.assertEqual('BareMetalNode', gen.get_class_name_from_file('bare-metal-node'))
        self.assertEqual('PhysicalMedium', gen.get_class_name_from_file('Physical-medium'))

    def test_convert_list_to_string(self):
        self.assertEqual('a, b, c', gen.convert_list_to_string(['a', 'b', 'c']))
        self.assertEqual('a, b, c', gen.convert_list_to_string(['a', 'b', 'c'], False))
        self.assertEqual('[a](#a), [b](#b), [c](#c)', gen.convert_list_to_string(['a', 'b', 'c'], True))

    def test_generate_outline(self):
        self.assertEqual('## Outline\n\n- [A](#a)\n- [B](#b)\n- [C](#c)\n\n\n', gen.generate_outline(['a', 'b', 'C']))

if __name__ == '__main__':
    unittest.main()

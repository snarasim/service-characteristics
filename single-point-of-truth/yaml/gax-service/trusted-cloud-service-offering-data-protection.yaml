TrustedCloudServiceOfferingDataProtection:
    subClassOf: ['TrustedCloudServiceOffering']
    prefix: 'gax-service'
    attributes:
        # 7.1
        -
            title: 'goodImplementationOfDSGVOMeasures'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'How do appropriate measures of the DSGVO measures get implemented?'
            exampleValues: [ 'No statement', 'Formally described', 'According to a contract', 'Certified measurements' ]
        -
            title: 'detailsAboutDSGVOMeasures'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Details about the implemented technical and organisational measurements'
            exampleValues: [ 'Freetext' ]

        # 7.2
        -
            title: 'fulfillmentOfFormalRequirements'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are the predescribed formal requirements fulfilled?'
            exampleValues: [ 'No statement', 'Requirements are fulfilled', 'Measurements are documented in the contract', 'Measurements are documented publicly', 'Proof via certificate' ]
        -
            title: 'contractedSupportOfDFA'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Has contracted support of a "data protection impact assessment" (DFA), if needed?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'contractedPseudonymizationOrEncryptionOfPersonalData'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is there contracted pseudonymization or encryption of personal data?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'contractedImplementationOfDataSubjectRights'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Has contracted implementation of data subject rights?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'contractedDeletionOfDataAndLinksInTheCloud'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Has a contracted deletion of files and there links in the cloud?'
            exampleValues: [ 'true', 'false' ]
        -
            title: 'typeOfPersonalDataAccordingToTheProvider'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Are declarations in the ADV-contract about personal data as well as categories of the affected persons?'
            exampleValues: [ 'true', 'false' ]

        # 7.3
        -
            title: 'proofsForTheCustomerAccordingToADVContract'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Which proofs according to the ADV-contract can be given to the customer for examination?'
            exampleValues: [ 'Self-audit', 'Internal behaviour rules with external proof', 'Certificate about data protection', 'Approved behaviour rules', 'Certificates after Art. 42 DS-GVO', 'Freetext' ]
        -
            title: 'registryAboutCustomerProcessingActivities'
            prefix: 'gax-service'
            dataType: 'xsd:boolean'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is a registry about the customers processing activities created according to Art. 30 EU-DSGVO?'
            exampleValues: [ 'true', 'false' ]

        # 7.4
        -
            title: 'possibleLocationRestrictionForCustomerData'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Can the hosting of customer data be restricted to a specific location?'
            exampleValues: [ 'No statement', 'No restriction', 'Restriction to a region', 'Restriction to the EU', 'Restriction to germany' ]
        -
            title: 'possibleLocationRestrictionForCustomerDataAdministration'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Can the administration of customer data be restricted to a specific location?'
            exampleValues: [ 'No statement', 'No restriction', 'Restriction to a region', 'Restriction to the EU', 'Restriction to germany' ]
        -
            title: 'geoRegionOptions'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Possible GEO-regions'
            exampleValues: [ 'Freetext' ]

        # 7.5
        -
            title: 'guaranteeForImplementationOfDataSubjectRights'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'How can the requirements of the DSGVO according to data subect rights be fulfilled, e.g. the deletion of personal data?'
            exampleValues: [ 'Freetext' ]
        -
            title: 'guaranteeToDeleteLinksToCustomerData'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'How can it be guaranteed, that also links get deleted, if the data is removed?'
            exampleValues: [ 'Freetext' ]

        # 7.6
        -
            title: 'contractedObligationForAllProcessingPersons'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Is there a contracted obligation for all the people processing personal data according to Art. 28 Abs. 3 S. 2 lit. b, 29, 32 Abs. 4 DSG-VO?'
            exampleValues: [ 'Freetext' ]

        # 7.7
        -
            title: 'dataProtectionCertificateName'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'Name of the data protection certificate'
            exampleValues: [ 'Freetext' ]
        -
            title: 'dataProtectionCertificateDetails'
            prefix: 'gax-service'
            dataType: 'xsd:string'
            # this is an optional attribute, hence key cardinality is missing
            description: 'More details about the certificate e.g. examination, extension and so on'
            exampleValues: [ 'Freetext' ]

import argparse

import csv
import check_yaml

# Command Line Interface for the script check_yaml
if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Validates the semantics of yaml files specified')
    sub_parser = parser.add_subparsers(title="Output to be generated", dest="output")

    # Setup parser for sd-attribute command
    y2j_parser = sub_parser.add_parser("semantic-validation",
                                       help="Creates a JSON (validation) file for each YAML (single source of truth) file")
    y2j_parser.add_argument('--srcFile', type=str, required=True,
                            help='Path to CSV file specifying where fo find specific files')
    y2j_parser.add_argument('--yamlFolder', type=str, required=True,
                            help='Path to yaml files specifying where fo find specific files')

    args = parser.parse_args()
    if args.output == "semantic-validation":

        yamlFolder: str = args.yamlFolder
        src_path: str = args.srcFile

        with open(src_path, encoding='utf-8') as file:
            csvreader = csv.reader(file)
            dict_from_csv = {}
            for row in csvreader:
                dict_from_csv[row[0]] = row[1]

        print(dict_from_csv)

        ssot_dta: str = dict_from_csv["SSOT_dataTypeAbbreviation_path"]
        ssot_prefixes: str = dict_from_csv["SSOT_prefixes"]
        semantic_val_mandatory_attributes: str = dict_from_csv["SEMANTIC_VALIDATION_MANDATORY_ATTRIBUTES"]

        check_yaml.execute_validation(yaml_folder=yamlFolder, prefix_file=ssot_prefixes,
                                      data_type_abbreviation_file=ssot_dta,
                                      semantic_val_mandatory_attributes=semantic_val_mandatory_attributes)

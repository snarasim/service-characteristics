```mermaid
classDiagram

class Resource{
    <<abstract>>
}

Resource <|-- PhysicalResource: 
Resource <|-- VirtualResource
VirtualResource "1" <-- "instanceOf" InstantiatedVirtualResource

PhysicalResource "1" ..> "0..*" Participant : ownedBy
PhysicalResource "1" ..> "0..*" Participant : manufacturedBy
PhysicalResource "1" ..> "1..*" Participant : maintainedBy

VirtualResource "1" ..> "1..*" Participant : copyrightOwnedBy

InstantiatedVirtualResource --> "1" Resource : hostedOn

InstantiatedVirtualResource "1" ..> "1..*" Participant : tenantOwnedBy
InstantiatedVirtualResource "1" ..> "1..*" Participant : maintainedBy
```
